use core::{
    mem::ManuallyDrop,
    sync::atomic::{self, AtomicI8, AtomicPtr},
};

fn compare_and_swap() {
    println!("==================== compare_and_swap ====================");
    let i = AtomicI8::new(5);
    let m = i.load(atomic::Ordering::SeqCst);
    println!("i 的初始值 = {}", m);
    let x = i.compare_exchange(5, 10, atomic::Ordering::SeqCst, atomic::Ordering::SeqCst);
    let n = i.load(atomic::Ordering::SeqCst);
    println!("i 修改后的值 = {}", n);
    println!("i compare_and_swap 后的返回值 = {:?}", x);
}

fn fetch_update() {
    println!("==================== fetch_update ====================");
    let i = AtomicI8::new(5);
    let m = i.load(atomic::Ordering::SeqCst);
    println!("i 的初始值 = {}", m);
    let x = i.fetch_update(atomic::Ordering::SeqCst, atomic::Ordering::SeqCst, |x| {
        if x == 5 {
            Some(10)
        } else {
            None
        }
    });
    let n = i.load(atomic::Ordering::SeqCst);
    println!("i fetch_update 后的值 = {}", n);
    println!("i fetch_update 后的返回值 = {:?}", x);
}

fn atomic_ptr() {
    println!("==================== AtomicPtr ====================");
    let mut hello_str = "hello".to_string();
    let p = AtomicPtr::new(&mut hello_str);
    let mut rust_str = "rust".to_string();
    p.store(&mut rust_str, atomic::Ordering::SeqCst);
    let c = p.load(atomic::Ordering::SeqCst);
    // 使用 ManuallyDrop 来避免双重释放
    let result = unsafe { ManuallyDrop::new(std::ptr::read(c)) };
    println!("p 修改前的值 = {}", hello_str);
    println!("p 修改后的值 = {}", *result);
}

fn main() {
    compare_and_swap();
    fetch_update();
    atomic_ptr();
}
