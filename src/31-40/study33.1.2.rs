use std::time::{SystemTime, UNIX_EPOCH};

fn time_stamp(msg: impl AsRef<str>) -> String {
    let time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis();
    time.to_string() + ": " + msg.as_ref()
}
/**
 * 出于性能考虑:
 *      如果首选类型是&str，使用AsRef<str> trait绑定，
 *      如果首选类型是String，使用Into<String> trait绑定。
 */
fn concat(msg1: impl Into<String>, msg2: impl AsRef<str>) -> String {
    msg1.into() + "; " + msg2.as_ref()
}

fn main() {
    let s1 = "msg as &str";
    let s2 = String::from("msg as String");
    let s3 = String::from("msg as String");
    let s4 = "msg as &str";
    println!("{}", time_stamp(s1));
    println!("{}", time_stamp(s2));
    println!("{}", concat(s3, s4));
}
