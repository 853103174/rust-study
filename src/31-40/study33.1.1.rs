use std::collections::HashMap;

fn lookup(map: &HashMap<String, i32>, key: &str) -> Result<i32, String> {
    map.get(key)
        .copied()
        .ok_or_else(|| format!("key {} not found", key))
}

fn main() {
    let mut map = HashMap::new();
    map.insert("x".to_string(), 1);
    assert_eq!(lookup(&map, "x"), Ok(1));
    assert_eq!(lookup(&map, "y"), Err("key y not found".to_string()));

    println!("{:?}", lookup(&map, "x"));
    println!("{:?}", lookup(&map, "y"));
}
