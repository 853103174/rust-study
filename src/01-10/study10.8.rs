#[derive(Debug)]
struct Highlight<'doc>(&'doc str);

fn erase(text: String) {
    println!("Bye {text}!");
}

fn main() {
    let text = String::from("The quick brown fox jumps over the lazy dog.");
    let fox = Highlight(&text[4..19]);
    let dog = Highlight(&text[35..43]);
    //如果 text 在 fox （或 dog ）的生存期结束之前被消耗，则借用检查器抛出错误。
    //erase(text);
    println!("{fox:?}");
    println!("{dog:?}");
}
