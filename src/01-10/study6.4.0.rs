fn main() {
    let a: [i32; 6] = [10, 20, 30, 40, 50, 60];
    println!("a: {a:?}");

    let s1: &[i32] = &a[2..4];
    println!("s1: {s1:?}");
    let s2: &[i32] = &a[..a.len()];
    println!("s2: {s2:?}");
    let s3: &[i32] = &a[2..];
    println!("s3: {s3:?}");
    let s4: &[i32] = &a[..];
    println!("s4: {s4:?}");
}
