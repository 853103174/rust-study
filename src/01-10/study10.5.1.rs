fn main() {
    let mut a: i32 = 10;
    let b: &i32 = &a;

    {
        //代码无法编译，因为 a 同时被借用为可变（通过 c ）和不可变（通过 b ）。
        let c: &mut i32 = &mut a;
        *c = 20;
    }

    println!("a: {a}");
    println!("b: {b}");
}
