fn main() {
    //默认不可变类型
    let x: i32 = 10;
    println!("x: {x}");

    //x = 20;
    //println!("x: {x}");
}
