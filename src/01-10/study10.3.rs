fn say_hello(name: String) {
    println!("Hello {name}");
}
fn main() {
    let name = String::from("Alice");
    say_hello(name);
    //所有权转移导致报错
    //say_hello(name);
}
