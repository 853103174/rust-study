use core::cell::RefCell;
use std::{collections::HashMap, sync::Mutex, thread};

use std::sync::Arc;

struct FibonacciCalculator {
    cache: RefCell<HashMap<usize, usize>>,
}
impl FibonacciCalculator {
    fn calculate(&self, n: usize) -> usize {
        if n <= 2 {
            return 1;
        }

        if let Some(value) = self.cache.borrow().get(&n) {
            return *value;
        }

        let result = self.calculate(n - 1) + self.calculate(n - 2);
        self.cache.borrow_mut().insert(n, result);

        result
    }
}

fn main() {
    let counter = Arc::new(Mutex::new(0));

    let counter1 = counter.clone();
    let handle1 = thread::spawn(move || {
        for _ in 0..10 {
            *counter1.lock().unwrap() += 1;
        }
    });

    let counter2 = counter.clone();
    let handle2 = thread::spawn(move || {
        for _ in 0..10 {
            *counter2.lock().unwrap() += 1;
        }
    });

    handle1.join().unwrap();
    handle2.join().unwrap();

    println!("Value: {}", *counter.lock().unwrap());
}
