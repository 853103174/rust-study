fn main() {
    let mut x = 10;
    x = if x % 2 == 0 { x / 2 } else { 3 * x + 1 };
    println!("x: {x}");
}
