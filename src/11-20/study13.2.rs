#[derive(Debug)]
struct Person {
    name: String,
    age: u8,
}

impl Default for Person {
    fn default() -> Self {
        Self {
            name: "Bot".to_string(),
            age: 0,
        }
    }
}

fn create_default() {
    let tmp = Person {
        ..Default::default()
    };
    println!("{tmp:?}");
    let tmp = Person {
        name: "Sam".to_string(),
        ..Default::default()
    };
    println!("{tmp:?}");
}

fn main() {
    create_default();
}
