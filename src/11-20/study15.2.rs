#[derive(Debug)]
struct Race {
    name: String,
    laps: Vec<i32>,
}

impl Race {
    //无接收器, 静态方法
    fn new(name: &str) -> Race {
        Race {
            name: String::from(name),
            laps: Vec::new(),
        }
    }

    //对自己的独占借用读写访问权限
    fn add_lap(&mut self, lap: i32) {
        self.laps.push(lap);
    }

    //共享和只读借用访问自己
    fn print_laps(&self) {
        println!("Recorded {} laps for {}:", self.laps.len(), self.name);
        for (idx, lap) in self.laps.iter().enumerate() {
            println!("Lap {idx}: {lap} sec");
        }
    }

    //自己的专属所有权
    fn finish(self) {
        let total = self.laps.iter().sum::<i32>();
        println!("Race {} is finished, total lap time: {}", self.name, total);
    }
}

fn main() {
    let mut race = Race::new("Monaco Grand Prix");
    race.add_lap(70);
    race.add_lap(68);
    race.print_laps();
    race.add_lap(71);
    race.print_laps();
    race.finish();
    //race.add_lap(42);
}
