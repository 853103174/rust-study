use std::collections::HashMap;

fn main() {
    let mut page_counts = HashMap::new();
    page_counts.insert("Adventures of Huckleberry Finn".to_string(), 207);
    page_counts.insert("Grimms' Fairy Tables".to_string(), 751);
    page_counts.insert("Pride and Prejudice".to_string(), 303);

    if !page_counts.contains_key("Les Miserables") {
        println!(
            "We know about {} books, but not Les Miserables.",
            page_counts.len()
        );
    }

    for book in ["Pride and Prejudice", "Alice's Adventure in Wonderland"] {
        match page_counts.get(book) {
            Some(count) => println!("{book}: {count} pages"),
            None => println!("{book} is unknown."),
        }
    }

    //如果没有找到任何值，请使用.entry()方法插入一个值
    for book in ["Pride and Prejudice", "Alice's Adventure in Wonderland"] {
        let page_count: &mut i32 = page_counts.entry(book.to_string()).or_insert(0);
        *page_count += 1;
    }

    println!("{page_counts:#?}");

    let pc1 = page_counts
        .get("Harry Potter and the Sorcerer's Stone ")
        .unwrap_or(&336);
    println!("pc1: {pc1}");
    println!("{page_counts:#?}");

    let pc2 = page_counts
        .entry("The Hunger Games".to_string())
        .or_insert(374);
    println!("pc2: {pc2}");
    println!("{page_counts:#?}");

    let page_from = HashMap::from([
        ("Harry Potter and Sorcerer's Stone".to_string(), 336),
        ("The Hunger Games".to_string(), 374),
    ]);
    println!("page_from: {page_from:#?}");
}
