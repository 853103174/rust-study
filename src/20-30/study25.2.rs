fn main() {
    let primes = vec![2, 3, 5, 7];
    let primes_squares = primes
        .into_iter()
        .map(|prime| prime * prime)
        .collect::<Vec<_>>();
    println!("{primes_squares:?}");
}
